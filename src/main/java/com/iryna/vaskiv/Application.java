package com.iryna.vaskiv;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        // bissnes
        logger1.trace("This is a trace message");
        // bissnes
        logger1.debug("This is a debug message");
        logger1.info("This is an info message");
        logger1.warn("This is a warn message");
        logger1.error("This is an error message");
        logger1.fatal("This is a fatal message");
    }
}