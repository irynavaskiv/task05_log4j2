package com.iryna.vaskiv;

import com.twilio.Twilio;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSSender {
    // Find your Account Sid and Auth Token at twilio.com/console
    public static final String ACCOUNT_SID =
            "ACea812742cecce92efedce35139874acf";
    public static final String AUTH_TOKEN =
            "ba4858098a86216b6d9c43dfa4420353";
    public static void main(String[] args) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380969864439"), // to
                        new PhoneNumber("+14693384576"), // from
                        "Log4j works with Ira Vaskiv!")
                .create();

        System.out.println(message.getSid());
    }
}
